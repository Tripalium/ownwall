EESchema Schematic File Version 2
LIBS:Power_converter_V4-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Gajda_opto
LIBS:IRS_Driver
LIBS:sensors
LIBS:arduino_shieldsNCL
LIBS:Measurement_block-cache
LIBS:Measurement_block-rescue
LIBS:digital
LIBS:lm2672
LIBS:digital-cache
LIBS:feeder_block-cache
LIBS:module_ttl_rs485
LIBS:buffer_chain-cache
LIBS:driver_block-cache
LIBS:driver_block-rescue
LIBS:measurement_chain-cache
LIBS:measurement_chain_LV-cache
LIBS:microcontroller_block-cache
LIBS:non inverting buffer_chain-cache
LIBS:power_block-cache
LIBS:power_block-rescue
LIBS:quad_shottky_x2
LIBS:Power_converter_V4-cache
LIBS:Power_converter_V4.2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 540  4080 0    79   ~ 0
LOW-SIDE VOLTAGE SENSING CHAIN
Text Notes 540  4280 0    59   ~ 0
Senses the voltage isolated from high power side.
Text Notes 800  650  0    60   ~ 0
Input Variables
Text Label 3150 1650 2    60   ~ 0
GNDLow
Text Label 4000 800  2    60   ~ 0
GNDPwR
Text Label 3100 1450 2    60   ~ 0
+5VLow
Text Label 3150 1850 2    60   ~ 0
+5VHigh
Text HLabel 2750 1650 0    60   Input ~ 0
GNDLow
Text HLabel 3600 800  0    60   Input ~ 0
GNDPwR
Text HLabel 2700 1450 0    60   Input ~ 0
+5VLow
Text HLabel 2750 1850 0    60   Input ~ 0
+5VHigh
Text Label 1350 1050 2    60   ~ 0
ViL-
Text HLabel 950  1050 0    60   Input ~ 0
ViL-
Text Label 1350 1450 2    60   ~ 0
ViH-
Text Label 1350 1250 2    60   ~ 0
ViH2+
Text HLabel 950  1450 0    60   Input ~ 0
ViH-
Text HLabel 950  1250 0    60   Input ~ 0
ViH+
Text Label 1300 1850 2    60   ~ 0
VL-
Text Label 1300 1650 2    60   ~ 0
VL+
Text HLabel 900  1850 0    60   Input ~ 0
VL-
Text HLabel 900  1650 0    60   Input ~ 0
VL+
Text Label 2200 1050 2    60   ~ 0
VH-
Text Label 2200 850  2    60   ~ 0
VH+
Text HLabel 1800 1050 0    60   Input ~ 0
VH-
Text HLabel 1800 850  0    60   Input ~ 0
VH+
Text Label 2200 1450 2    60   ~ 0
VT1-
Text Label 2200 1250 2    60   ~ 0
VT1+
Text HLabel 1800 1450 0    60   Input ~ 0
VT1-
Text HLabel 1800 1250 0    60   Input ~ 0
VT1+
Text Notes 11000 915  2    60   ~ 0
Output Variables
Text Label 10250 1215 0    60   ~ 0
miso
Text HLabel 10650 1215 2    60   Output ~ 0
miso
Text Notes 3770 5915 0    79   ~ 0
HIGH-SIDE CURRENT SENSING CHAIN
Text Notes 3770 6065 0    59   ~ 0
Senses the current isolated from high power side.
Text Notes 3770 3940 0    79   ~ 0
LOW-SIDE CURRENT SENSING CHAIN
Text Notes 3770 4090 0    59   ~ 0
Senses the current isolated from high power side.
Text Notes 640  6130 0    79   ~ 0
HIGH-SIDE VOLTAGE SENSING CHAIN
Text Notes 690  6280 0    59   ~ 0
Senses the voltage isolated from high power side.
Text Notes 610  2285 0    79   ~ 0
TRANSISTOR TEMPERATURE SENSING CHAIN
Text Notes 660  2485 0    59   ~ 0
Senses the voltage issued from the temperature sensor.
Text Notes 7590 3940 0    79   ~ 0
A/D CONVERTER AND DIGITAL ISOLATOR UNIT
Text Notes 7325 4425 0    59   ~ 0
The ADC is done through an isolated chain which is implemented by two \ncomponents. On the left is the 12bit ADC and on the right is the digital\nisolator. The communication with the microcontroller is done through SPI. \nTwo 10k resistors are used on both sides to simplify the system.
$Comp
L MCP3208-RESCUE-Power_converter_V4 UADC1
U 1 1 5975F2C8
P 7995 5550
F 0 "UADC1" H 7745 6050 50  0000 R CNN
F 1 "MCP3208" H 8645 4950 50  0000 R CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 8095 5650 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21298c.pdf" H 8095 5650 50  0001 C CNN
	1    7995 5550
	1    0    0    -1  
$EndComp
$Comp
L ISO7341C Diso1
U 1 1 5975F2CF
P 9845 5550
F 0 "Diso1" H 9595 6150 50  0000 C CNN
F 1 "ISO7341C" H 9545 4950 50  0000 C CNN
F 2 "SMD_Packages:SO-16-W" H 8945 4950 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/iso7340c.pdf" H 9845 5950 50  0001 C CNN
	1    9845 5550
	-1   0    0    -1  
$EndComp
Text Label 11190 5250 2    60   ~ 0
GNDLow
Text Label 7645 6450 2    60   ~ 0
GNDPwR
Text Label 10700 4700 0    60   ~ 0
+5VLow
Text Label 7445 4700 2    60   ~ 0
+5VHigh
Text Label 8870 5250 2    60   ~ 0
GNDPwR
NoConn ~ 7395 5950
Text Label 7095 5450 0    48   ~ 0
VmiL+
Text Label 7095 5650 0    48   ~ 0
VmiH+
Text Label 7095 5350 0    48   ~ 0
VmL+
Text Label 7095 5750 0    48   ~ 0
VmH+
Text Label 7095 5550 0    48   ~ 0
VmT1+
Text Label 10985 5650 2    60   ~ 0
clk
Text Label 10645 5750 2    60   ~ 0
mosi
Text Label 10645 5850 2    60   ~ 0
ss
Text Label 10645 5950 2    60   ~ 0
miso
Text Label 6070 4915 0    60   ~ 0
VmiL+
Text Label 5020 4365 2    60   ~ 0
+5VHigh
$Comp
L R Rg1
U 1 1 5975FBC8
P 4770 5215
F 0 "Rg1" V 4850 5215 50  0000 C CNN
F 1 "10k" V 4770 5215 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4700 5215 50  0001 C CNN
F 3 "" H 4770 5215 50  0000 C CNN
	1    4770 5215
	1    0    0    -1  
$EndComp
$Comp
L C Cf1
U 1 1 5975FBE1
P 4570 5115
F 0 "Cf1" V 4620 5165 50  0000 L CNN
F 1 "100n" V 4620 4915 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 4608 4965 50  0001 C CNN
F 3 "" H 4570 5115 50  0000 C CNN
	1    4570 5115
	-1   0    0    1   
$EndComp
Text Label 4095 5440 2    60   ~ 0
ViL-
Text Label 5220 5515 2    60   ~ 0
GNDPwR
Text Label 5995 6940 0    60   ~ 0
VmiH+
Text Label 4945 6390 2    60   ~ 0
+5VHigh
Text Label 4020 6840 2    60   ~ 0
ViH2+
$Comp
L R Rg3
U 1 1 597603B8
P 4695 7240
F 0 "Rg3" V 4775 7240 50  0000 C CNN
F 1 "10k" V 4695 7240 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4625 7240 50  0001 C CNN
F 3 "" H 4695 7240 50  0000 C CNN
	1    4695 7240
	1    0    0    -1  
$EndComp
$Comp
L C Cf2
U 1 1 597603D1
P 4495 7140
F 0 "Cf2" V 4545 7190 50  0000 L CNN
F 1 "100n" V 4545 6940 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 4533 6990 50  0001 C CNN
F 3 "" H 4495 7140 50  0000 C CNN
	1    4495 7140
	-1   0    0    1   
$EndComp
$Comp
L R Rf4
U 1 1 597603D8
P 4295 6840
F 0 "Rf4" V 4375 6840 50  0000 C CNN
F 1 "150k" V 4295 6840 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4225 6840 50  0001 C CNN
F 3 "" H 4295 6840 50  0000 C CNN
	1    4295 6840
	0    -1   -1   0   
$EndComp
Text Label 4020 7490 2    60   ~ 0
ViH-
Text Label 5145 7490 2    60   ~ 0
GNDPwR
Text Label 2540 5155 0    60   ~ 0
VmL+
Text Label 1690 4605 2    60   ~ 0
+5VHigh
Text Label 1140 5055 2    60   ~ 0
VL+
Text Label 2715 7080 0    60   ~ 0
VmH+
Text Label 1115 6980 2    60   ~ 0
VH+
Text Label 2685 3285 0    60   ~ 0
VmT1+
Text Label 1835 2785 2    60   ~ 0
+5VHigh
Text Label 1210 3185 2    60   ~ 0
VT1+
Text Label 2950 850  2    60   ~ 0
clk
Text HLabel 2550 850  0    60   Input ~ 0
clk
Text Label 2950 1050 2    60   ~ 0
ss
Text HLabel 2550 1050 0    60   Input ~ 0
ss
Text Label 3050 1250 2    60   ~ 0
mosi
Text HLabel 2650 1250 0    60   Input ~ 0
mosi
Text Label 4000 1000 2    60   ~ 0
Pgood
Text HLabel 3600 1000 0    60   Input ~ 0
Pgood
Text Label 7095 5250 0    60   ~ 0
Pgood
Text Label 1835 3735 0    60   ~ 0
GNDPwR
Text Label 1690 5605 0    60   ~ 0
GNDPwR
Text Label 1865 7605 0    60   ~ 0
GNDPwR
Text Label 1350 900  2    60   ~ 0
ViL2+
Text HLabel 950  900  0    60   Input ~ 0
ViL+
Text Label 4095 4815 2    60   ~ 0
ViL2+
$Comp
L R Rf1
U 1 1 5975FBE8
P 4370 4815
F 0 "Rf1" V 4450 4815 50  0000 C CNN
F 1 "150k" V 4370 4815 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4300 4815 50  0001 C CNN
F 3 "" H 4370 4815 50  0000 C CNN
	1    4370 4815
	0    1    1    0   
$EndComp
$Comp
L CONN_01X01 TM1
U 1 1 599F4D1D
P 6925 1200
F 0 "TM1" H 6925 1300 50  0000 C CNN
F 1 "CONN_01X01" V 7025 1200 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6925 1200 50  0001 C CNN
F 3 "" H 6925 1200 50  0000 C CNN
	1    6925 1200
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TM2
U 1 1 599F4E13
P 6925 1450
F 0 "TM2" H 6925 1550 50  0000 C CNN
F 1 "CONN_01X01" V 7025 1450 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6925 1450 50  0001 C CNN
F 3 "" H 6925 1450 50  0000 C CNN
	1    6925 1450
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TM3
U 1 1 599F4E96
P 7825 1185
F 0 "TM3" H 7825 1285 50  0000 C CNN
F 1 "CONN_01X01" V 7925 1185 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 7825 1185 50  0001 C CNN
F 3 "" H 7825 1185 50  0000 C CNN
	1    7825 1185
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TM4
U 1 1 599F4F13
P 7825 1435
F 0 "TM4" H 7825 1535 50  0000 C CNN
F 1 "CONN_01X01" V 7925 1435 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 7825 1435 50  0001 C CNN
F 3 "" H 7825 1435 50  0000 C CNN
	1    7825 1435
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TM5
U 1 1 599F4F9A
P 8680 1200
F 0 "TM5" H 8680 1300 50  0000 C CNN
F 1 "CONN_01X01" V 8780 1200 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 8680 1200 50  0001 C CNN
F 3 "" H 8680 1200 50  0000 C CNN
	1    8680 1200
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TM6
U 1 1 599F5C5B
P 8680 1450
F 0 "TM6" H 8680 1550 50  0000 C CNN
F 1 "CONN_01X01" V 8780 1450 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 8680 1450 50  0001 C CNN
F 3 "" H 8680 1450 50  0000 C CNN
	1    8680 1450
	-1   0    0    1   
$EndComp
Text Label 7325 1200 0    48   ~ 0
VmiL+
Text Label 7325 1450 0    48   ~ 0
VmiH+
Text Label 8225 1185 0    48   ~ 0
VmL+
Text Label 8225 1435 0    48   ~ 0
VmH+
Text Label 9080 1200 0    48   ~ 0
VmT1+
Text Label 9080 1450 0    60   ~ 0
Pgood
$Comp
L C_Small Cd6
U 1 1 59B29B19
P 8995 5075
F 0 "Cd6" H 9005 5145 50  0000 L CNN
F 1 "100n" H 9005 4995 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 8995 5075 50  0001 C CNN
F 3 "" H 8995 5075 50  0001 C CNN
	1    8995 5075
	1    0    0    -1  
$EndComp
$Comp
L C_Small Cd7
U 1 1 59B29BA0
P 10995 5045
F 0 "Cd7" H 11005 5115 50  0000 L CNN
F 1 "100n" H 11005 4965 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 10995 5045 50  0001 C CNN
F 3 "" H 10995 5045 50  0001 C CNN
	1    10995 5045
	1    0    0    -1  
$EndComp
$Comp
L C_Small Cd5
U 1 1 59B2AF71
P 8395 4900
F 0 "Cd5" H 8405 4970 50  0000 L CNN
F 1 "100n" H 8405 4820 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 8395 4900 50  0001 C CNN
F 3 "" H 8395 4900 50  0001 C CNN
	1    8395 4900
	0    1    1    0   
$EndComp
Text Label 5845 6415 2    60   ~ 0
GNDPwR
$Comp
L C_Small Cd3
U 1 1 59B2B545
P 5345 6515
F 0 "Cd3" H 5355 6585 50  0000 L CNN
F 1 "100n" H 5355 6435 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 5345 6515 50  0001 C CNN
F 3 "" H 5345 6515 50  0001 C CNN
	1    5345 6515
	0    1    1    0   
$EndComp
$Comp
L C_Small Cd2
U 1 1 59B2BCAC
P 1965 4680
F 0 "Cd2" H 1975 4750 50  0000 L CNN
F 1 "100n" H 1975 4600 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 1965 4680 50  0001 C CNN
F 3 "" H 1965 4680 50  0001 C CNN
	1    1965 4680
	0    1    1    0   
$EndComp
$Comp
L C_Small Cd1
U 1 1 59B2BE95
P 2060 2835
F 0 "Cd1" H 2070 2905 50  0000 L CNN
F 1 "100n" H 2070 2755 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 2060 2835 50  0001 C CNN
F 3 "" H 2060 2835 50  0001 C CNN
	1    2060 2835
	0    1    1    0   
$EndComp
Text Label 2240 4680 0    60   ~ 0
GNDPwR
Text Label 2360 2835 0    60   ~ 0
GNDPwR
Text Label 1790 6655 2    60   ~ 0
+5VHigh
NoConn ~ 7395 5850
Text Notes 3795 2235 0    79   ~ 0
CURRENT MEASUREMENT OFFSET
Text Notes 3845 2385 0    59   ~ 0
Implements a current offset
Text Label 5870 3185 0    60   ~ 0
Voffset
Text Label 5080 3540 0    60   ~ 0
GNDPwR
Text Label 4945 2760 2    60   ~ 0
+5VHigh
$Comp
L R Rg2
U 1 1 5ABF3153
P 5420 5265
F 0 "Rg2" V 5500 5265 50  0000 C CNN
F 1 "150k" V 5420 5265 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5350 5265 50  0001 C CNN
F 3 "" H 5420 5265 50  0000 C CNN
	1    5420 5265
	0    1    1    0   
$EndComp
$Comp
L R Rg4
U 1 1 5ABF3932
P 5345 7290
F 0 "Rg4" V 5425 7290 50  0000 C CNN
F 1 "150k" V 5345 7290 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5275 7290 50  0001 C CNN
F 3 "" H 5345 7290 50  0000 C CNN
	1    5345 7290
	0    1    1    0   
$EndComp
$Comp
L OPA2374 U2
U 1 1 5ABFAC6C
P 5320 4915
F 0 "U2" H 5320 5065 50  0000 L CNN
F 1 "OPA2374" H 5320 4765 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5220 4965 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa2374.pdf" H 5320 5065 50  0001 C CNN
	1    5320 4915
	1    0    0    -1  
$EndComp
$Comp
L OPA2374 U2
U 2 1 5ABFAE43
P 1790 5155
F 0 "U2" H 1790 5305 50  0000 L CNN
F 1 "OPA2374" H 1790 5005 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 1690 5205 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa2374.pdf" H 1790 5305 50  0001 C CNN
	2    1790 5155
	1    0    0    -1  
$EndComp
$Comp
L OPA2374 U3
U 1 1 5ABFB0AA
P 5245 6940
F 0 "U3" H 5245 7090 50  0000 L CNN
F 1 "OPA2374" H 5245 6790 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5145 6990 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa2374.pdf" H 5245 7090 50  0001 C CNN
	1    5245 6940
	1    0    0    -1  
$EndComp
$Comp
L OPA2374 U3
U 2 1 5ABFB4B6
P 1965 7080
F 0 "U3" H 1965 7230 50  0000 L CNN
F 1 "OPA2374" H 1965 6930 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 1865 7130 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa2374.pdf" H 1965 7230 50  0001 C CNN
F 4 "??" H 1965 7080 60  0001 C CNN "Ref. RS"
	2    1965 7080
	1    0    0    -1  
$EndComp
$Comp
L OPA374 U1
U 1 1 5ABFBCF2
P 1935 3285
F 0 "U1" H 1985 3485 50  0000 C CNN
F 1 "OPA374" H 2135 3085 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 1885 3385 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa2374.pdf" H 1985 3485 50  0001 C CNN
	1    1935 3285
	1    0    0    -1  
$EndComp
$Comp
L OPA374 U4
U 1 1 5ABFC1E0
P 5120 3185
F 0 "U4" H 5170 3385 50  0000 C CNN
F 1 "OPA374" H 5320 2985 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5070 3285 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa2374.pdf" H 5170 3385 50  0001 C CNN
	1    5120 3185
	1    0    0    -1  
$EndComp
NoConn ~ 1915 3025
NoConn ~ 5100 2925
$Comp
L C_Small Cd4
U 1 1 5ABFCE63
P 5410 2785
F 0 "Cd4" H 5420 2855 50  0000 L CNN
F 1 "100n" H 5420 2705 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 5410 2785 50  0001 C CNN
F 3 "" H 5410 2785 50  0001 C CNN
	1    5410 2785
	0    1    1    0   
$EndComp
Text Label 5710 2785 0    60   ~ 0
GNDPwR
$Comp
L MCP1541 Ref1
U 1 1 5ABFE4F2
P 5590 1330
F 0 "Ref1" H 5750 970 50  0000 C CNN
F 1 "MCP1541" H 5460 1660 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 5800 1420 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21653C.pdf" H 5800 1420 50  0001 C CNN
F 4 "?" H 5590 1330 60  0001 C CNN "Ref. RS"
	1    5590 1330
	1    0    0    -1  
$EndComp
Text Label 4470 1140 2    60   ~ 0
+5VHigh
Text Label 6145 1330 0    60   ~ 0
GNDPwR
$Comp
L R Rof2
U 1 1 5AC0081B
P 4115 2850
F 0 "Rof2" V 4195 2850 50  0000 C CNN
F 1 "150k" V 4115 2850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4045 2850 50  0001 C CNN
F 3 "" H 4115 2850 50  0000 C CNN
	1    4115 2850
	1    0    0    -1  
$EndComp
$Comp
L POT-RESCUE-Power_converter_V4 Pof1
U 1 1 5AC019F0
P 4115 3295
F 0 "Pof1" V 4015 3295 50  0000 C CNN
F 1 "10k" V 4115 3295 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Bourns_3296Y_3-8Zoll_Angular_ScrewUp" H 4115 3295 50  0001 C CNN
F 3 "" H 4115 3295 50  0000 C CNN
	1    4115 3295
	1    0    0    -1  
$EndComp
Text Label 4025 2615 2    60   ~ 0
+VRef
Text Label 4115 3700 2    60   ~ 0
GNDPwR
Text Notes 4170 720  0    79   ~ 0
VOLTAGE REFERENCE
Text Notes 4220 870  0    59   ~ 0
Provides a stable 4,096V reference
Text Label 4920 1550 2    60   ~ 0
+VRef
Text Label 7625 4930 2    60   ~ 0
+VRef
Text Notes 7525 775  0    79   ~ 0
MEASUREMENT TEST POINTS
Text Notes 7260 965  0    59   ~ 0
Pins used for measuring while debugging
Text Label 4100 6535 2    60   ~ 0
Voffset
Text Label 4175 4515 2    60   ~ 0
Voffset
$Comp
L CONN_01X01 TM7
U 1 1 5AC4F865
P 8685 1700
F 0 "TM7" H 8685 1800 50  0000 C CNN
F 1 "CONN_01X01" V 8785 1700 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 8685 1700 50  0001 C CNN
F 3 "" H 8685 1700 50  0000 C CNN
	1    8685 1700
	-1   0    0    1   
$EndComp
Text Label 9085 1700 0    60   ~ 0
Voffset
$Comp
L R Ris2
U 1 1 5AC50453
P 9195 5450
F 0 "Ris2" V 9275 5450 50  0000 C CNN
F 1 "10k" V 9195 5450 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 9125 5450 50  0001 C CNN
F 3 "" H 9195 5450 50  0000 C CNN
	1    9195 5450
	1    0    0    -1  
$EndComp
$Comp
L R Ris4
U 1 1 5AC52191
P 10610 5525
F 0 "Ris4" V 10540 5525 50  0000 C CNN
F 1 "10k" V 10610 5525 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 10540 5525 50  0001 C CNN
F 3 "" H 10610 5525 50  0000 C CNN
	1    10610 5525
	0    -1   -1   0   
$EndComp
$Comp
L R Rf2
U 1 1 5AC53050
P 4505 4515
F 0 "Rf2" V 4585 4515 50  0000 C CNN
F 1 "150k" V 4505 4515 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4435 4515 50  0001 C CNN
F 3 "" H 4505 4515 50  0000 C CNN
	1    4505 4515
	0    1    1    0   
$EndComp
$Comp
L R Rf3
U 1 1 5AC5351E
P 4405 6535
F 0 "Rf3" V 4485 6535 50  0000 C CNN
F 1 "150k" V 4405 6535 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4335 6535 50  0001 C CNN
F 3 "" H 4405 6535 50  0000 C CNN
	1    4405 6535
	0    -1   -1   0   
$EndComp
$Comp
L R Rof1
U 1 1 5AC56515
P 3940 2845
F 0 "Rof1" V 4020 2845 50  0000 C CNN
F 1 "150k" V 3940 2845 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3870 2845 50  0001 C CNN
F 3 "" H 3940 2845 50  0000 C CNN
	1    3940 2845
	1    0    0    -1  
$EndComp
Text Notes 6630 2260 0    79   ~ 0
CURRENT MEASUREMENT
Text Notes 6630 2675 0    59   ~ 0
The current measurement chain has three elements:\n- An input filter for the signal \n- An offset for bi-directional current measurement\n- A gain relative to the shunt value 
Text Notes 6610 3190 0    59   ~ 0
INPUT RC FILTER\n--------\nR = 150k\nC = 100n\nfc = 66.66Hz
Text Notes 7585 3450 0    59   ~ 0
INPUT OFFSET\n----------\nResistor bridge\nUpper part - 75kOhm\nLower part - 10kOhm \n               Trimmer\nRANGE - 0V - 481mV\nTARGET OFFSET - 325mV
Text Notes 8835 3695 0    59   ~ 0
GAIN - Non-inverter (1 + (Rg2/Rg1) )\n----------\nInput voltage step = 13mV per Amp\nInput Current Range = -20A to +20A\nOutput current step = 100mV per Amp\nOutput Meas. Voltage range = 0.5V to 4.5V\nGain = 100mV/13mV = 7.63\nRration = (7.63 * 2) - 1 = 14.38\nRg1 = 10k\nRg2 = 150k\nOffset = 2.5V/7.65 = 325mV\n
$Comp
L R Ris1
U 1 1 5AC5BAF0
P 9030 5470
F 0 "Ris1" V 9110 5470 50  0000 C CNN
F 1 "10k" V 9030 5470 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8960 5470 50  0001 C CNN
F 3 "" H 9030 5470 50  0000 C CNN
	1    9030 5470
	1    0    0    -1  
$EndComp
$Comp
L R Ris3
U 1 1 5AC5D139
P 10610 5405
F 0 "Ris3" V 10690 5405 50  0000 C CNN
F 1 "10k" V 10610 5405 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 10540 5405 50  0001 C CNN
F 3 "" H 10610 5405 50  0000 C CNN
	1    10610 5405
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4520 4815 5020 4815
Wire Wire Line
	4570 4965 4570 4815
Wire Notes Line
	490  4180 2790 4180
Wire Wire Line
	2750 1650 3150 1650
Wire Wire Line
	3600 800  4000 800 
Wire Wire Line
	2700 1450 3100 1450
Wire Wire Line
	2750 1850 3150 1850
Wire Wire Line
	950  1050 1350 1050
Wire Wire Line
	950  1450 1350 1450
Wire Wire Line
	950  1250 1350 1250
Wire Wire Line
	900  1850 1300 1850
Wire Wire Line
	900  1650 1300 1650
Wire Wire Line
	1800 1050 2200 1050
Wire Wire Line
	1800 850  2200 850 
Wire Wire Line
	1800 1450 2200 1450
Wire Wire Line
	1800 1250 2200 1250
Wire Wire Line
	10650 1215 10250 1215
Wire Notes Line
	4050 1950 4050 650 
Wire Notes Line
	3720 5965 6020 5965
Wire Notes Line
	3720 3990 6020 3990
Wire Notes Line
	640  6180 2940 6180
Wire Notes Line
	810  2385 3110 2385
Wire Notes Line
	700  700  1500 700 
Wire Notes Line
	10250 965  11050 965 
Wire Notes Line
	7390 4040 9690 4040
Wire Wire Line
	8920 5950 9445 5950
Wire Wire Line
	8830 5750 9445 5750
Wire Wire Line
	8830 5650 8830 5750
Wire Wire Line
	8595 5650 8830 5650
Wire Wire Line
	9445 5850 8735 5850
Wire Wire Line
	8735 5850 8735 5750
Wire Wire Line
	8735 5750 8595 5750
Wire Wire Line
	8595 5550 8920 5550
Wire Wire Line
	8920 5550 8920 5950
Wire Wire Line
	7445 4700 9195 4700
Wire Wire Line
	8195 4700 8195 5050
Connection ~ 8195 4700
Wire Wire Line
	7895 6150 7895 6350
Wire Wire Line
	7895 6350 8195 6350
Wire Wire Line
	8195 6350 8195 6150
Wire Wire Line
	8045 6350 8045 6450
Connection ~ 8045 6350
Wire Wire Line
	10245 5150 10835 5150
Wire Wire Line
	10400 4700 10700 4700
Connection ~ 10400 5150
Wire Wire Line
	10245 5250 11190 5250
Wire Wire Line
	10245 5350 10420 5350
Wire Wire Line
	8870 5250 9445 5250
Wire Wire Line
	9445 5350 9295 5350
Wire Wire Line
	9295 5350 9295 5250
Connection ~ 9295 5250
Wire Wire Line
	9195 5150 9445 5150
Wire Wire Line
	8965 5450 8965 5690
Wire Wire Line
	8595 5450 8965 5450
Connection ~ 9195 5150
Wire Wire Line
	10245 5650 10985 5650
Wire Wire Line
	10245 5750 10645 5750
Wire Wire Line
	10245 5850 10645 5850
Wire Wire Line
	10245 5950 10645 5950
Wire Wire Line
	7095 5250 7395 5250
Wire Wire Line
	7095 5350 7395 5350
Wire Wire Line
	7095 5450 7395 5450
Wire Wire Line
	7095 5550 7395 5550
Wire Wire Line
	7095 5650 7395 5650
Wire Wire Line
	5620 4915 6070 4915
Wire Wire Line
	5220 4365 5220 4615
Wire Wire Line
	5220 5215 5220 5515
Wire Wire Line
	4770 5015 5020 5015
Wire Wire Line
	4770 5015 4770 5065
Wire Wire Line
	5220 4365 5020 4365
Wire Wire Line
	4970 5015 4970 5265
Wire Wire Line
	4970 5265 5270 5265
Connection ~ 4970 5015
Wire Wire Line
	5570 5265 5820 5265
Wire Wire Line
	5820 5265 5820 4915
Connection ~ 5820 4915
Wire Wire Line
	4770 5440 4770 5365
Connection ~ 4570 4815
Wire Wire Line
	4570 5265 4570 5440
Wire Wire Line
	4095 5440 4770 5440
Connection ~ 4570 5440
Wire Wire Line
	5545 6940 5995 6940
Wire Wire Line
	5145 6390 5145 6640
Wire Wire Line
	5145 7240 5145 7490
Wire Wire Line
	4695 7040 4945 7040
Wire Wire Line
	4695 7040 4695 7090
Wire Wire Line
	4445 6840 4945 6840
Wire Wire Line
	5145 6390 4945 6390
Wire Wire Line
	4895 7040 4895 7290
Wire Wire Line
	4895 7290 5195 7290
Connection ~ 4895 7040
Wire Wire Line
	5495 7290 5745 7290
Wire Wire Line
	5745 7290 5745 6940
Connection ~ 5745 6940
Wire Wire Line
	4695 7490 4695 7390
Wire Wire Line
	4495 6840 4495 6990
Connection ~ 4495 6840
Wire Wire Line
	4495 7290 4495 7490
Wire Wire Line
	4020 7490 4695 7490
Connection ~ 4495 7490
Wire Wire Line
	2090 5155 2540 5155
Wire Wire Line
	1690 4605 1690 4855
Wire Wire Line
	1490 5255 1390 5255
Wire Wire Line
	1390 5255 1390 5660
Wire Wire Line
	1390 5660 2240 5660
Wire Wire Line
	2240 5660 2240 5155
Connection ~ 2240 5155
Wire Wire Line
	1140 5055 1490 5055
Wire Wire Line
	2265 7080 2715 7080
Wire Wire Line
	1665 7180 1565 7180
Wire Wire Line
	1565 7180 1565 7660
Wire Wire Line
	1565 7660 2415 7660
Wire Wire Line
	2415 7660 2415 7080
Connection ~ 2415 7080
Wire Wire Line
	1115 6980 1665 6980
Wire Wire Line
	2235 3285 2685 3285
Wire Wire Line
	1835 2785 1835 2985
Wire Wire Line
	1835 3735 1835 3585
Wire Wire Line
	1635 3385 1535 3385
Wire Wire Line
	1535 3385 1535 3780
Wire Wire Line
	1535 3780 2385 3780
Wire Wire Line
	2385 3780 2385 3285
Connection ~ 2385 3285
Wire Wire Line
	1210 3185 1635 3185
Wire Notes Line
	500  1950 4050 1950
Wire Notes Line
	9795 4700 9795 6300
Wire Notes Line
	9895 4700 9895 6300
Wire Wire Line
	8045 6450 7645 6450
Wire Notes Line
	11200 615  10000 615 
Wire Notes Line
	10000 615  10000 1515
Wire Notes Line
	10000 1515 11150 1515
Wire Wire Line
	2550 850  2950 850 
Wire Wire Line
	2550 1050 2950 1050
Wire Wire Line
	2650 1250 3050 1250
Wire Wire Line
	3600 1000 4000 1000
Wire Wire Line
	7395 5750 7095 5750
Wire Wire Line
	950  900  1350 900 
Wire Wire Line
	7125 1200 7325 1200
Wire Wire Line
	7125 1450 7325 1450
Wire Wire Line
	8025 1185 8225 1185
Wire Wire Line
	8025 1435 8225 1435
Wire Wire Line
	8880 1200 9080 1200
Wire Wire Line
	8880 1450 9080 1450
Wire Wire Line
	4145 6840 4020 6840
Wire Wire Line
	4095 4815 4220 4815
Wire Wire Line
	10400 4700 10400 5150
Wire Wire Line
	9195 4700 9195 5300
Wire Wire Line
	8995 5175 8995 5250
Connection ~ 8995 5250
Wire Wire Line
	8995 4975 8995 4900
Wire Wire Line
	8995 4900 9195 4900
Connection ~ 9195 4900
Wire Wire Line
	10400 4900 10995 4900
Connection ~ 10400 4900
Wire Wire Line
	8495 4900 8895 4900
Wire Wire Line
	8895 4900 8895 5250
Connection ~ 8895 5250
Wire Wire Line
	8295 4900 8195 4900
Connection ~ 8195 4900
Wire Wire Line
	5245 6515 5145 6515
Connection ~ 5145 6515
Wire Wire Line
	5445 6515 5845 6515
Wire Wire Line
	5845 6515 5845 6415
Wire Wire Line
	1960 2835 1835 2835
Connection ~ 1835 2835
Wire Wire Line
	2160 2835 2360 2835
Wire Wire Line
	1865 4680 1690 4680
Connection ~ 1690 4680
Wire Wire Line
	2065 4680 2240 4680
Wire Wire Line
	1690 5605 1690 5455
Wire Wire Line
	1790 6655 1865 6655
Wire Wire Line
	1865 6655 1865 6780
Wire Wire Line
	1865 7605 1865 7380
Wire Notes Line
	3795 2285 6095 2285
Wire Wire Line
	5420 3185 5870 3185
Wire Wire Line
	4820 3285 4720 3285
Wire Wire Line
	4720 3285 4720 3585
Wire Wire Line
	4720 3585 5570 3585
Wire Wire Line
	5570 3585 5570 3185
Connection ~ 5570 3185
Wire Wire Line
	4115 3085 4820 3085
Wire Wire Line
	4945 2760 5020 2760
Wire Wire Line
	5020 2760 5020 2885
Wire Wire Line
	5510 2785 5710 2785
Wire Wire Line
	5310 2785 5020 2785
Connection ~ 5020 2785
Wire Wire Line
	4470 1140 5150 1140
Wire Wire Line
	6145 1330 6010 1330
Wire Wire Line
	4920 1550 5150 1550
Wire Wire Line
	7625 4930 7895 4930
Wire Wire Line
	7895 4930 7895 5050
Wire Wire Line
	4115 3000 4115 3145
Connection ~ 4115 3085
Wire Wire Line
	4115 2615 4115 2700
Wire Wire Line
	4115 2615 4025 2615
Wire Wire Line
	4115 3445 4115 3700
Wire Wire Line
	5020 3485 5020 3540
Wire Wire Line
	5020 3540 5080 3540
Wire Wire Line
	4265 3295 4265 3085
Connection ~ 4265 3085
Wire Notes Line
	4170 770  6470 770 
Wire Notes Line
	7080 830  9380 830 
Wire Wire Line
	4830 6840 4830 6535
Wire Wire Line
	4830 6535 4555 6535
Connection ~ 4830 6840
Wire Wire Line
	4865 4815 4865 4515
Wire Wire Line
	4865 4515 4655 4515
Connection ~ 4865 4815
Wire Wire Line
	8885 1700 9085 1700
Wire Wire Line
	4175 4515 4355 4515
Wire Wire Line
	4100 6535 4255 6535
Wire Wire Line
	3940 2995 3940 3030
Wire Wire Line
	3940 3030 4115 3030
Connection ~ 4115 3030
Wire Wire Line
	3940 2695 3940 2645
Wire Wire Line
	3940 2645 4115 2645
Connection ~ 4115 2645
Wire Wire Line
	10420 5350 10420 5250
Connection ~ 10420 5250
Wire Notes Line
	6580 2310 8880 2310
Wire Wire Line
	9030 5320 9030 5275
Wire Wire Line
	9030 5275 9195 5275
Connection ~ 9195 5275
Wire Wire Line
	9030 5620 9030 5630
Wire Wire Line
	9030 5630 9410 5630
Wire Wire Line
	9410 5630 9410 5450
Wire Wire Line
	9410 5450 9445 5450
Wire Wire Line
	9195 5600 9195 5630
Connection ~ 9195 5630
Wire Wire Line
	8965 5690 9425 5690
Wire Wire Line
	9425 5690 9425 5650
Wire Wire Line
	9425 5650 9445 5650
Wire Wire Line
	10245 5450 10460 5450
Wire Wire Line
	10460 5450 10460 5405
Wire Wire Line
	10460 5525 10385 5525
Wire Wire Line
	10385 5525 10385 5450
Connection ~ 10385 5450
Wire Wire Line
	10835 5525 10760 5525
Wire Wire Line
	10835 5150 10835 5525
Wire Wire Line
	10760 5405 10835 5405
Connection ~ 10835 5405
Wire Wire Line
	10995 4900 10995 4945
Wire Wire Line
	10995 5145 10995 5250
Connection ~ 10995 5250
$Comp
L C_Small Cd8
U 1 1 5AC60CB1
P 4585 1315
F 0 "Cd8" H 4595 1385 50  0000 L CNN
F 1 "100n" H 4595 1235 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 4585 1315 50  0001 C CNN
F 3 "" H 4585 1315 50  0001 C CNN
	1    4585 1315
	-1   0    0    1   
$EndComp
Wire Wire Line
	4585 1215 4585 1140
Connection ~ 4585 1140
Wire Wire Line
	4585 1415 4585 1600
Text Label 4585 1600 2    60   ~ 0
GNDPwR
$Comp
L C Cd9
U 1 1 5AC64277
P 5085 1740
F 0 "Cd9" H 5110 1840 50  0000 L CNN
F 1 "4u7" H 5110 1640 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 5123 1590 50  0001 C CNN
F 3 "" H 5085 1740 50  0000 C CNN
	1    5085 1740
	1    0    0    -1  
$EndComp
Wire Wire Line
	5085 1590 5085 1550
Connection ~ 5085 1550
Wire Wire Line
	5085 1890 5085 1955
Text Label 5085 1955 2    60   ~ 0
GNDPwR
Text Notes 7050 6925 0    59   ~ 0
BUG Comments\n----------\nThere's a net error with ViL+ and ViH+. To compensate, these are \ntransformed into ViL2+ and ViH2+, respectively. 
Text Notes 7350 7500 0    79   ~ 0
MEASUREMENT BLOCK
$EndSCHEMATC
